# README #

These are UMA DCS Wardrobe files for WillB's UMA RPG Starter Pack.

### Requirements ###
* UMA - https://www.assetstore.unity3d.com/en/#!/content/35611
* UMA RPG Starter Pack - https://www.assetstore.unity3d.com/en/#!/content/45732

### How do I get set up? ###
* Add the Wardrobe folder to your Unity project
* Drag the folder onto the UMA Asset Indexer 
* Open UMA's demo scene "UMA DCS Demo - Simple Setup"